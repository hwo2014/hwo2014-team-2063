(ns hwo2014bot.speed
  (:require [hwo2014bot.track :as t]
            [hwo2014bot.helpers :as h]))

(def speed-history (list {:tick 0
                          :in-piece-distance 0
                          :piece-index 0
                          :distance-raced 0
                          :speed 0}))

(defn calculate-distance-raced [track-pieces lap piece-index in-piece-distance]
  (let [lap (* lap t/lap-distance) 
        not-finished-lap (t/calculate-distance (take (inc piece-index) track-pieces))]
    (+ lap not-finished-lap in-piece-distance)))

(defn calculate-speed [distance-raced]
  (- distance-raced (:distance-raced (first speed-history))))

(defn manage-speed-history [msg track-pieces tick]
  (let [piece-position (:piecePosition (h/own-car-data (:data msg)))
        in-piece-distance (:inPieceDistance piece-position)
        piece-index (:pieceIndex piece-position)
        lap (:lap piece-position)
        distance-raced (calculate-distance-raced track-pieces lap piece-index in-piece-distance)
        speed (calculate-speed distance-raced)]
    (if (< speed 0)
      (first speed-history)
      (let [new-speed-history (cons {:in-piece-distance in-piece-distance, :tick tick, :distance-raced distance-raced, :piece-index piece-index :speed speed} speed-history)]
        (def speed-history new-speed-history)
        (first speed-history)))))
