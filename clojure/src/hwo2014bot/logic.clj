(ns hwo2014bot.logic
  (:require [hwo2014bot.speed :as s]
            [hwo2014bot.helpers :as h]
            [hwo2014bot.track :as t]))

(def number-of-crashes 0)

(def target-speed-modifier [1.0])
(defn guess-target-speed [piece next-piece-index]
  (let [angle (h/convert-to-positive (:angle piece))
        radius (:radius piece)]
    (let [const (/ angle radius)
          piece-modifier (get t/track-piece-speed-modifier (keyword (str next-piece-index)) 1.0)
          target-speed (cond 
                         (< const 0.1) 10.0
                         (< const 0.2) 9.0
                         (< const 0.3) 8.5
                         (< const 0.35) 8.0
                         (< const 0.4) 7.0
                         (< const 0.45) 6.0
                         (< const 0.5) 5.0
                         (< const 0.6) 4.0
                         (< const 0.7) 3.0
                         (< const 0.8) 2.5
                         (< const 0.9) 2.0
                         (< const 1.0) 1.5
                         :else 1.0)]
      (* target-speed piece-modifier (first target-speed-modifier)))))

(defn modify-target-speed-modifier [msg game-tick]
  (def number-of-crashes (inc number-of-crashes))
  (let [rest-modifiers (rest target-speed-modifier)]
    (when (seq rest-modifiers)
      (def target-speed-modifier rest-modifiers)))
  (when (> number-of-crashes 3)
    (def target-speed-modifier (cons (* (first target-speed-modifier) 0.8) target-speed-modifier))
  (t/modify-track-piece-speed-modifier s/speed-history)
  {:msgType "ping" :data "ping"})

(defn decide-straight-throttle [piece-index track-pieces]
  (let [next-next-piece (nth track-pieces (mod (+ 2 piece-index)
                                               (count track-pieces)))]
    (if (nil? (:angle next-next-piece))
      1.0
      (let [target-speed (guess-target-speed next-next-piece (+ 2 piece-index))
            speed (:speed (first s/speed-history))
            delta (- speed target-speed)]
        (cond
          (< speed 1) 1.0
          (> delta 2.5) 0.5
          :else 1.0)))))

(defn decide-throttle [msg track-pieces tick]
  (s/manage-speed-history msg track-pieces tick)
  (let [car-data (h/own-car-data (:data msg))
        piece-index (:pieceIndex (:piecePosition car-data))
        next-piece (nth track-pieces (inc piece-index) (first track-pieces))
        next-piece-angle (h/convert-to-positive (:angle next-piece))]
    (if (not (nil? next-piece-angle))
      (let [speed (:speed (first s/speed-history))
            target-speed (guess-target-speed next-piece (inc piece-index))
            delta (- speed target-speed)]
        (cond
          (< speed 1) 0.5 ;if speed is really slow, give 0.5 throttle
          (> delta 1) 0.001
          (> delta -0.5) 0.5
          :else 1.0))
      (let [straight-throttle (decide-straight-throttle piece-index track-pieces)]
        straight-throttle))))
        
(defn get-piece-position [msg]
  (:piecePosition (:data msg)))

(defn get-own-car-piece-index [msg]
  (:pieceIndex (:piecePosition (h/own-car-data (:data msg)))))

(defn get-car-lane-index [msg]
  (:endLaneIndex (:lane (:piecePosition (h/own-car-data (:data msg))))))

(defn straight? [msg track-pieces]
  (let [car-data (h/own-car-data (:data msg))
        piece-index (:pieceIndex (:piecePosition car-data))
        no-angle? (nil? (:angle (nth track-pieces piece-index)))]
    no-angle?))

(defn handle-car-position [msg game-tick]
  (let [throttle (decide-throttle msg t/track-pieces game-tick)
        switch? (:switch (nth t/track-pieces (get-own-car-piece-index msg)))]
    (if (and
          switch? straight?
          (< (:in-piece-distance (first s/speed-history)) 10))
      (let [lane-index (get-car-lane-index msg)]
        (if (< lane-index (dec t/lane-count))
          {:msgType "switchLane" :data "Right"}
          {:msgType "switchLane" :data "Left"}))
      (if (not (nil? game-tick))
        {:msgType "throttle" 
         :data throttle
         :gameTick game-tick}
        {:msgType "throttle" 
         :data throttle}))))

(defn handle-lap-finished [msg game-tick]
  (when (zero? number-of-crashes)
    (def target-speed-modifier (cons (+ (first target-speed-modifier) 0.1) target-speed-modifier))
  {:msgType "ping" :data "ping" :gameTick game-tick})
