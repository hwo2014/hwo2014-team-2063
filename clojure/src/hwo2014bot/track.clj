(ns hwo2014bot.track
  (:require [hwo2014bot.helpers :as h]))

(def track-pieces [])
(def track-piece-speed-modifier {})
(def lane-count nil)

(defn modify-track-piece-speed-modifier [speed-history]
  (def track-piece-speed-modifier 
    (let [piece-index-key (keyword (str (dec (:piece-index (first speed-history)))))
          new-modifier (- (get track-piece-speed-modifier piece-index-key 1.0) 0.3)
          non-negative-mod (if (< new-modifier 0.0) 0.0 new-modifier)]
      (assoc track-piece-speed-modifier piece-index-key non-negative-mod))))

(defn angle-piece-length [piece]
  (let [angle (h/convert-to-positive (:angle piece))
        radius (h/convert-to-positive (:radius piece))]
    (* (* (/ 3.14 180) angle) radius)))

(defn calculate-distance [track-pieces]
  (defn inner-loop [distance pieces]
    (if (empty? pieces)
      distance
      (if (not (nil? (:length (first pieces))))
        (inner-loop (+ distance (:length (first pieces))) (rest pieces))
        (recur (+ distance (angle-piece-length (first pieces))) (rest pieces)))))
  (inner-loop 0.0 track-pieces))

(def lap-distance 0.0)
(defn get-track-pieces [msg]
  (def track-pieces (:pieces (:track (:race (:data msg)))))
  (def lap-distance (calculate-distance track-pieces))
  (def track-piece-speed-modifier (zipmap (map (comp keyword str) (range)) (map (fn [ignore] 1.1) track-pieces)))
  (def lane-count (count (:lanes (:track (:race (:data msg))))))
  (println "Lane Count is:" lane-count)
  {:msgType "ping" :data "ping"})
