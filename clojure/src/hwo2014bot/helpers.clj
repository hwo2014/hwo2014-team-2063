(ns hwo2014bot.helpers)

(defn convert-to-positive [n]
  (if (and (number? n) (< n 0))
    (* n -1)
    n))

(defn own-car-data [data]
  (let [car-data (first data)]
    (if (= (:name (:id car-data)) "In the meantime")
      car-data
      (recur (rest data)))))
