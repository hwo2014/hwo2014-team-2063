(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.logic :as l]
            [hwo2014bot.track :as t])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn handle-msg [msg game-tick]
  (let [ret-msg (case (:msgType msg)
    "carPositions" (l/handle-car-position msg game-tick)
    "gameInit" (t/get-track-pieces msg)
    "crash" (l/modify-target-speed-modifier msg game-tick)
    "lapFinished" (l/handle-lap-finished msg game-tick)
    ;"gameStart" (l/handle-game-start msg track-pieces game-tick)
    {:msgType "ping" :data "ping"})]
    ;(println ret-msg)
    ret-msg))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println msg)
    "gameStart" (println msg)
    "crash" (println msg)
    "gameEnd" (println msg)
    "error" (println msg)
    "finish" (println msg)
    "dnf" (println msg)
    "lapFinished" (println msg)
    "gameInit" (println msg)
    "turboAvailable" (println msg)
    ;"carPositions" (println msg)
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (let [game-tick (:gameTick msg)]
      (send-message channel (handle-msg msg game-tick))
      (recur channel))))

(defn create-join-message 
  ([botname botkey] {:msgType "join" :data {:name botname :key botkey}})
  ([botname botkey track] {:msgType "joinRace" 
                          :data {
                                 :botId {:name botname :key botkey} 
                                 :trackName track}}))

(defn -main[& [host port botname botkey track]]
  (println host port botname botkey track)
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (if (nil? track)
      (send-message channel (create-join-message botname botkey))
      (send-message channel (create-join-message botname botkey track)))
    (game-loop channel)))
