(ns hwo2014bot.logic_test
  (:use clojure.test)
  (:require [hwo2014bot.logic :as l]
            [hwo2014bot.track :as t]))

(def track-pieces-test [{:length 100.0} {:length 100.0} {:length 100.0} {:switch true, :length 100.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :switch true, :angle 22.5} {:length 100.0} {:length 100.0} {:radius 200, :angle -22.5} {:length 100.0} {:switch true, :length 100.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:switch true, :length 100.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :angle 22.5} {:radius 200, :angle -22.5} {:switch true, :length 100.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:length 62.0} {:radius 100, :switch true, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:switch true, :length 100.0} {:length 100.0} {:length 100.0} {:length 100.0} {:length 90.0}])

(def msg {:msgType "carPositions", :data [{:id {:name "In the meantime", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 0.0, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}], :gameId "f92120d1-a502-49c7-9d21-fa3fde1309c8"})

(deftest test-decide-throttle
         (let [result (l/decide-throttle msg track-pieces-test)]
           (is (map? result))
           (is (not (nil? (:msgType result))))
           (is (= (:msgType result) "throttle"))
           (is (not (nil? (:data result))))
           (is (= (:data result) 1.0))))
