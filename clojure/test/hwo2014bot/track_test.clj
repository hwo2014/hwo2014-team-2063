(ns hwo2014bot.track_test
  (:use clojure.test)
  (:require [hwo2014bot.logic :as l]
            [hwo2014bot.track :as t]))

(def game-init-message {:msgType "gameInit", :data {:race {:track {:id "keimola", :name "Keimola", :pieces [{:length 100.0} {:length 100.0} {:length 100.0} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :angle 22.5, :switch true} {:length 100.0} {:length 100.0} {:radius 200, :angle -22.5} {:length 100.0} {:length 100.0, :switch true} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:radius 100, :angle -45.0} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 200, :angle 22.5} {:radius 200, :angle -22.5} {:length 100.0, :switch true} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:length 62.0} {:radius 100, :angle -45.0, :switch true} {:radius 100, :angle -45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:radius 100, :angle 45.0} {:length 100.0, :switch true} {:length 100.0} {:length 100.0} {:length 100.0} {:length 90.0}], :lanes [{:distanceFromCenter -10, :index 0} {:distanceFromCenter 10, :index 1}], :startingPoint {:position {:x -300.0, :y -44.0}, :angle 90.0}}, :cars [{:id {:name "In the meantime", :color "red"}, :dimensions {:length 40.0, :width 20.0, :guideFlagPosition 10.0}}], :raceSession {:laps 3, :maxLapTimeMs 60000, :quickRace true}}}, :gameId "72996590-a9b1-4e2d-9604-084e57dd955d"})

(defn atleast-one-elem-has-key? [seq-of-maps key]
  (let [bool-list (map #(not (nil? (key %1))) seq-of-maps)
        int-list (map #(if %1 1 0) bool-list)]
    (> (reduce + int-list) 0)))

(deftest get-track-pieces-test
         (let [result (t/get-track-pieces game-init-message)]
           (is (vector? result))
           (is (> (count result) 0))
           (is (atleast-one-elem-has-key? result :radius))
           (is (atleast-one-elem-has-key? result :length))
           (is (atleast-one-elem-has-key? result :switch))
           (is (atleast-one-elem-has-key? result :angle))))
